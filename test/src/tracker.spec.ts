import { random } from "faker";
import { expect } from "chai";
import { get } from "request-promise-native"
import { dbQuery, syncDB } from "./utils/db";

const TRACKER_HOST = process.env['TRACKER_HOST'] || 'localhost';
const TRACKER_PORT = process.env['TRACKER_PORT'] || 3000;

describe('Tracker Server', function() {
  this.timeout(30000);
  
  before(function(done) {
    setTimeout(done, 15000);
  });

  beforeEach(async function() {
    await syncDB();
    
    // reset database
    await dbQuery({ type: "remove", table: "track_events" });

    // feed data
    this.tracks = (await dbQuery({
      type: "insert",
      table: "track_events",
      values: {
        "rider_id": 4,
        "north": random.number({ min: 1, max: 20 }),
        "south": random.number({ min: 1, max: 20 }),
        "west": random.number({ min: 1, max: 20 }),
        "east": random.number({ min: 1, max: 20 }),
        "createdAt": new Date(),
        "updatedAt": new Date()
      },
      returning: [ "rider_id", "north",
        "south", "west", "east", "createdAt"
      ]
    }))[0][0];
  });

  describe('Movement', function() {
    it('harusnya memberikan data suatu rider', async function() {
      const response = await get(
        `http://${TRACKER_HOST}:${TRACKER_PORT}/movement/4`,
        { json: true }
      );
      expect(response.ok).to.be.true;
    });
  });
});
