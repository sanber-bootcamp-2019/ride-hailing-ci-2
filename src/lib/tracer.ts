import { initTracer, TracingOptions, TracingConfig } from "jaeger-client";

export function createTracer(serviceName: string) {
  const config: TracingConfig = {
    serviceName,
    sampler: {
      type: "const",
      param: 1
    },
    disable: process.env["TEST"] ? true : false,
    reporter: {
      agentHost: process.env["JAEGER_AGENT_HOST"] || "localhost",
      agentPort: process.env["JAEGER_AGENT_PORT"]
        ? parseInt(process.env["JAEGER_AGENT_PORT"], 10)
        : 6832,
      logSpans: true
    }
  };
  const options: TracingOptions = {
    logger: {
      info(msg) {
        console.log("INFO ", msg);
      },
      error(msg) {
        console.log("ERROR", msg);
      }
    }
  };
  return initTracer(config, options);
}
